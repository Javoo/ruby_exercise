#I´ve implemented (at least tried) two solutions. First one is not as efficient as second, but it works#
#At the end you will find some tests ive made to not lose time on changing the parameters, but you are free to do it!#


#SOLUTION 1 - #
def ItemSum(array, target)
  (0..array.length-1).each do |i|           #Bucle se ejecuta en cada item
    (i..array.length-1).each do |j|         #Bucle anidado para comprobar el resto respecto al item |i|
        if array[i] + array[j] == target    #Comprobación
          return [i, j]                          #p = puts + inspect, se printea de una forma más bonita
          break
        elsif array[i] == target            #Si llegamos al target simplemente con un solo item...
          return [i]
          break
        end
    end
  end
end


#SOLUTION 2#
def ItemSum2(array, target)
  while target > 0
    (0...array.length).each_with_index do |i, index|
      target = target - i
      if target <= 0
        return [index, i]
        break
      end
    end
  end
end

puts "\nSOLUTION 1\n----------------"
#Mofify the array and target inside the function call
p ItemSum([2,7,11,15], 13)
puts "----------------\n"


puts "\nSOLUTION 2\n----------------"
#Mofify the array and target inside the function call
p ItemSum2([2,7,11,15], 9)
puts "----------------\n"



#TESTS#
puts "\nTESTS for SOLUTION 1\n----------------"
puts "The output should be TRUE and its --> " + "#{ItemSum([2,7,11,15], 13) == [0, 2]}" + "\n"    #SHOULD BE TRUE
puts "The output should be TRUE and its --> " + "#{ItemSum([2,7,11,15], 9) == [0, 1]}" + "\n"     #SHOULD BE TRUE
puts "The output should be TRUE and its --> " + "#{ItemSum([2,7,11,15], 7) == [1]}" + "\n"        #SHOULD BE TRUE
puts "The output should be FALSE and its --> " + "#{ItemSum([2,7,11,15], 18) == [2, 3]}" + "\n"   #SHOULD BE FALSE
puts "The output should be TRUE and its --> " + "#{ItemSum([2,7,11,15], 2) == [0]}" + "\n"        #SHOULD BE TRUE
puts "The output should be TRUE and its --> " + "#{ItemSum([2,7,11,15], 26) == [2, 3]}" + "\n"    #SHOULD BE TRUE
puts "The output should be TRUE and its --> " + "#{ItemSum([2,7,11,15], 17) == [0, 3]}" + "\n"    #SHOULD BE TRUE
puts "The output should be FALSE and its --> " + "#{ItemSum([2,7,11,15], 9) == [0, 2]}" + "\n"    #SHOULD BE FALSE